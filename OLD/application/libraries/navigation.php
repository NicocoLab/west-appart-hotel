<?php if (!defined('BASEPATH'))	exit ('No direct script access allowed');

class Navigation {

	public function __construct() {
		$this->CI = & get_instance();
	}
	
	// -------------------------------------------------------------------------
	
	function display($menu = array())
	{
		if (count($menu) == 0)
		{
			return;
		}
		
		$nav = '<div id="navigation"><h2>Menu</h2><ul>';
		
		foreach ($menu as $k => $detail)
		{
			$nav .= '<li '.($detail['actif'] === TRUE ? 'class="actif"' : '').'>'.anchor($this->CI->uri->segment(1).'/'.$detail['lien'], $detail['lib']).'</li>';
		}
		
		$nav .= '</ul></div>';
		
		return $nav;
	}
	
		
}