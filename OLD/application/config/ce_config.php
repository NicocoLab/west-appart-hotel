<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['vads_site_id']			= 86714670;
$config['vads_currency']		= 978;
$config['vads_ctx_mode']		= 'PRODUCTION';
$config['vads_page_action']		= 'PAYMENT';
$config['vads_action_mode']		= 'INTERACTIVE';
$config['vads_payment_config'] 	= 'SINGLE';
$config['vads_version']			= 'V2';
$config['vads_return_mode']		= 'POST';

$config['vads_order_info']		= sha1(SECURITY_KEY);
