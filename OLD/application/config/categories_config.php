<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['categories'] = array(
			1 => 'Studio',
			2 => 'Appartement T3',
			3 => 'Salle de séminaire',
			4 => 'Salle de réception',
			5 => 'Studio accessibilité handicapés',
		);