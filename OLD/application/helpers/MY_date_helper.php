<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	// --------------------------------------------------------------------

	if ( ! function_exists('date_from_db'))
	{
		function date_from_db($date = '', $hour = FALSE)
		{

			if ($date == '')
			{
				return;
			}
			
			if ($hour === FALSE)
			{
				$format = 'd/m/Y';
			}
			else
			{
				$format = 'd/m/Y H:i:s';
			}
			
			$date = date_create($date);
			if ( ! $date) {
				 $e = date_get_last_errors();
				 foreach ($e['errors'] as $error) {
					  echo "$error\n";
				 }
				 exit();
			}

			return date_format($date, $format);
		}
	}
	
	// --------------------------------------------------------------------
	
	if ( ! function_exists('date_to_db'))
	{
		function date_to_db($date = '', $format = '', $hour = FALSE)
		{
			if ($date == '' or $format == '')
			{
				return;
			}

			$info = date_parse_from_format($format, $date);
			
			$ret = $info['year'].'-'.$info['month'].'-'.$info['day'];
			
			if ($hour === TRUE)
			{
				$ret.= ($info['hour'] == '' ? '00' : $info['hour']).':'.($info['minute'] == '' ? '00' : $info['minute']).':'.($info['second'] == '' ? '00' : $info['second']);
			}
			
			return $ret;
		}
	}
	
	// --------------------------------------------------------------------
	
	if ( ! function_exists('is_date'))
	{
		function is_date($str)
		{
			if (strtotime($str))
			{
				return TRUE; 
			}
			else
			{
				return FALSE;
			}
		} 
	}
