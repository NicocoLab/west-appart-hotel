<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	// --------------------------------------------------------------------

	if ( ! function_exists('make_trans_id'))
	{
		function make_trans_id()
		{
			$filename = SITE_ROOT.'application/config/count.txt';
			$fp = fopen($filename, 'r+');
			flock($fp, LOCK_EX);
			
			// lecture/incrémentation
			$count = (int)fread($fp, 6);
			$count++;
			
			if ($count < 0 || $count > 899999)
			{
				$count = 0;
			}
			
			// on revient au début du fichier
			fseek($fp, 0);
			ftruncate($fp,0);
			
			// écriture/fermeture/Fin du lock
			fwrite($fp, $count);
			flock($fp, LOCK_UN);
			fclose($fp);
			
			// le trans_id : on rajoute des 0 au début si nécessaire
			$trans_id = sprintf("%06d",$count);
			
			// Retour
			return $trans_id;
		}
	}
	
	// --------------------------------------------------------------------

	if ( ! function_exists('total_ttc_resa'))
	{
		function total_ttc_resa($resa)
		{
			$total_ttc = 0;
			
			foreach ($resa->chambres as $c)
			{
				$total_ht  = $c->prix * $resa->duree;
				$total_ttc += $total_ht * (1 + ($c->taux_tva/100));
			}
			
			return number_format($total_ttc, 2, '.', '') * 100;
		}
	}
	
	// --------------------------------------------------------------------

	if ( ! function_exists('make_signature'))
	{
		function make_signature($params)
		{
			// Tri des paramètres par ordre alphabétique
			ksort($params); 
			
			// Chaine de la signature
			$contenu_signature = '';
			foreach ($params as $nom => $valeur)
			{
				$contenu_signature .= $valeur.'+';
			}
			
			// On ajoute le certificat à la fin
			$contenu_signature .= CE_CERTIFICAT;
			
			// Retour
			return sha1($contenu_signature);
		}
	}