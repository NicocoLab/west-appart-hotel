<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->format_date = 'd/m/Y';
	}
	
	// -------------------------------------------------------------------------
    
	function myloader($page = '', $data = '')
	{ 	
		// On charge les vues
		$this->load->view('global/head', array( 'menu' => isset($data['menu']) ? $data['menu'] : array()));
		$this->load->view($page, $data);
		$this->load->view('global/foot');
	}
	
	// -------------------------------------------------------------------------
    
	function my_loader($pages = '', $data = '')
	{ 	
		if ( ! is_array($pages))
		{
			$pages[] = $pages;
		}
		
		// On charge les vues
		$this->load->view('hotel_niort_bessines/head');
		foreach ($pages as $k => $page)
		{
			$this->load->view($page, $data);
		}
		$this->load->view('hotel_niort_bessines/foot');
	}
	
	// -------------------------------------------------------------------------
}