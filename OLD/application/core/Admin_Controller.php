<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		// Identification utilisateur
		if ( ! $this->session->userdata('is_logged'))
		{
			$this->session->set_userdata('tmp_url', current_url());
			redirect('authentification');
		}
		
		$this->load->model('global/mdl_user');
		$this->load->helper('date');
		$this->load->library('navigation');
		
		// Chargement info utilisateur
		$this->user = $this->mdl_user->get_infos($this->session->userdata('user_id'));
		
		// Test pour l'encodage
		echo ( ! function_exists('mcrypt_encrypt')) ? 'Il faut installer php5-mcrypt' : '';
	}
	
	
	// -------------------------------------------------------------------------
   /**
    *
    * Fonction myloader
    * 
    * Charge les vues d'en-tete et de pied automatiquement
    *
    */
    
   function myloader($page = '', $data = '')
   { 	
   	// On charge les vues
   	$this->load->view('backoffice/head', array( 'menu' => isset($data['menu']) ? $data['menu'] : array()));
		$this->load->view($page, $data);
   	$this->load->view('backoffice/foot');
   }
	
}