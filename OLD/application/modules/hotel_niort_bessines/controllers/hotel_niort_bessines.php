<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class hotel_niort_bessines extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	// -------------------------------------------------------------------------
	
	function index ()
	{
		//$this->my_loader(array('accueil', 'les_appart', 'reservations', 'loisirs', 'seminaires', 'contact'));
		$this->my_loader(array('accueil', 'les_appart', 'reservations', 'contact'));
	}

	// -------------------------------------------------------------------------
	
	function contact()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('nom', 'Nom', 'required');
		$this->form_validation->set_rules('prenom', '', 'optional');
		$this->form_validation->set_rules('societe', '', 'optional');
		$this->form_validation->set_rules('adresse', '', 'optional');
		$this->form_validation->set_rules('telephone', '', 'optional');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('message', 'Message', 'required');
  
		if ( ! $this->form_validation->run())
		{
			echo $this->load->view('form', array(), TRUE);
		}
		else
		{
			$msg = 'Nouveau message depuis www.west-appart-hotel.com :<br /><br />';
			$msg.= '- Nom : '.$this->input->post('nom').'<br />';
			$msg.= '- Prénom : '.$this->input->post('prenom').'<br />';
			$msg.= '- Société : '.$this->input->post('societe').'<br />';
			$msg.= '- Adresse :<br />'.$this->mynl2br($this->input->post('adresse')).'<br />';
			$msg.= '- Téléphone : '.$this->input->post('telephone').'<br />';
			$msg.= '- E-mail : '.$this->input->post('email').'<br />';
			$msg.= '- message :<br />'.$this->mynl2br($this->input->post('message')).'<br />';
			
			$this->load->library('email');
			$this->email->from('contact@west-appart-hotel.com', "West Appart'Hôtel");
			$this->email->to(MAIL_CONTACT);
			$this->email->subject('Nouveau message depuis www.west-appart-hotel.com');
			$this->email->message($msg);
			
			if ($this->email->send())
			{
				$this->email->to('nico.riv@free.fr');
				$this->email->send();
				echo $this->load->view('form_ok', array(), TRUE);
			}
			else
			{
				//echo $this->email->print_debugger(); exit;
				echo $this->load->view('form_ko', array(), TRUE);
			}
		}
	}
	
	// -------------------------------------------------------------------------
	
	function fallback()
	{
		$this->load->view('fallback');
	}
	
	// -------------------------------------------------------------------------
	
	function mynl2br($text)
	{
		$text = nl2br($text);
		$text = str_replace("\r", '', $text);
		$text = str_replace("\n", '', $text);
		$text = str_replace("\r\n", '', $text);
		
		return $text;
	}
}
