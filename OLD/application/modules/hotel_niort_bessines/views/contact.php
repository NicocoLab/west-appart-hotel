<li id="contact">
	<div class="scrollDetectMiddle"></div>
	<h1 class="center">Nous <span>contacter</span></h1>
	
	<div class="innerWrapper">
		
		<div id="plan" class="corner shadow">
			<h2>Plan d'accès à l'hôtel</h2>
			<a href="<?php echo base_url(); ?>design/wah/hotel-plan-large.jpg" class="fancybox" title="Agrandir le plan">
				<img src="<?php echo base_url(); ?>design/wah/hotel-plan.jpg" alt="Plan d'accès à l'hôtel" class="pl" />
				<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
			</a>
			<p>
				Coordonnées GPS : N 46° 17.478' – W 000° 30.512'
				<br />
				140 Route de La Rochelle - 79000 Bessines
				<br />
				05 49 76 08 98 ou le 07 86 67 02 50 - <a href="mailto:contact@west-appart-hotel.com">contact@west-appart-hotel.com</a>
			</p>
		</div>
		
		<div id="contact_form" class="shadow corner"></div>
		
		
	</div>
</li>
