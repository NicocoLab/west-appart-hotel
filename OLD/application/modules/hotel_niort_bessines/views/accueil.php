<li id="accueil">
	<div class="scrollDetectMiddle"></div>
	<h1 class="center">Bienvenue <span>chez vous</span></h1>
	<div class="copy_wraper">
		<div class="room">
			<img src="<?php echo base_url()?>design/charte/accueil_appart_hotel.jpg" alt="Accueil Appart Hotel" class="shadow" />
		</div>
		<div class="room">
			<p class="shadow">
				<span>West Appart'Hotel</span> a été conçu pour vous accueillir et vous faciliter la vie lors de vos séjours, qu’ils soient professionnels ou de loisirs.
				<br /><br />
				<span>West Appart'Hotel</span> apporte une réponse parfaitement adaptée aux besoins croissants des entreprises à héberger leurs collaborateurs en déplacement, mais également aux particuliers à forte mobilité et aux touristes en famille ou entre amis...
				<br /><br />
				Ce nouveau concept d'hébergement en court ou long séjour est classé en Gamme Très Bon Confort par CléVacances.
				<br /><br />
				L'idée : Vous sentir comme <span>chez vous</span> et en toute autonomie !
			</p>
		</div>
		<div class="room">
			<img src="<?php echo base_url()?>design/charte/accueil_hotel.jpg" alt="Découvrez nos appartements T3" class="shadow" />
		</div>
	</div>
</li>
