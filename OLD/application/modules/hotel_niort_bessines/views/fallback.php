<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>West Appart'Hotel - Oooooops</title>
<style type="text/css">
body {
	background-color: #333;
	margin-left: 100px;
	margin-top: 100px;
	margin-right: 100px;
	margin-bottom: 100px;
	border: 0px;
	line-height: 22px;
}
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #FFF;
	border: 0px;
	width: 60%;
	margin: 0 auto
}
a:link {
	color: #CCC;
	border: 0px;
}
a:visited {
	color: #999;
	border: 0px;
}
a:hover {
	color: #999;
	border: 0px;
}
a:active {
	color: #333;
	border: 0px;
}
#logo {background: url(<?php echo base_url(); ?>design/wah/wah-sprite.png) 0 0 no-repeat; display: block; width: 186px; height: 21px; margin-top: 25px;}
#logo span {display: none}
h1 {
	font-size: 50px;
	color: #FFF;
}
h1,h2,h3,h4,h5,h6 {
	font-weight: bold;
}
</style>
</head>

<body>

<h1><strong>Oooooops!</strong><br />


</h1>
	<hr>
	<br />
	Vous ne pouvez pas voir www.west-appart-hotel.com ?
	<br /><br />
	C'est parce ce site utilise des technologies récentes, qui requière un navigateur récent. Pour une navigation optimale, merci d'utiliser <a href="http://www.apple.com/fr/safari/" target="_blank">Safari</a>, <a href="http://www.google.fr/chrome/" target="_blank">Chrome</a> or <a href="http://www.mozilla.org/fr/firefox/new/" target="_blank">Firefox</a>. Merci !
	<br /><br />
	<hr>
	
	<br /><br />
  
	<a href="<?php echo base_url(); ?>" id="logo" rel="1"><span>West Appart Hotel</span></a>
  
</body>
</html>
