<!DOCTYPE html>
<html>
	<head>
	
		<link rel="icon" type="image/png" href="<?php echo base_url(); ?>design/charte/favicon.png" />
	
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
		
		<link href="<?php echo base_url(); ?>design/wah.css?v20171601" rel="stylesheet" type="text/css" media="all" />
		
		<title>West Appart'Hotel - Niort - La Rochelle</title>
		
		<!--[if lt IE 8]>
			<meta http-equiv="refresh" content="0;url=<?php echo base_url(); ?>hotel_niort_bessines/fallback" />
		<![endif]-->
		
	</head>
	
	<body>
	
		<div id="header">
			<div class="innerWrapper">

				<!--
				<div id="login" class="corner-bottom">
					Espace client		
				</div>
				-->
				
				<a href="#accueil" id="logo"><span>West Appart<font>'</font>Hotel</span></a>
				<div id="coo">
					140 Route de La Rochelle - 79000 Bessines<br>
					<font>05 49 76 08 98</font> ou le <font>07 86 67 02 50</font>
				</div>
				
				<div id="nav">
					<ul>
						<li><a href="#accueil" class="actif">Bienvenue !</a></li>
						<li><a href="#apparts">Les West Appart'</a></li>
						<!--<li><a href="#services" rel="3">Services sur site</a></li>-->
						<li><a href="#reservations">Réservations</a></li>
						<!--<li><a href="#loisirs">4 - Loisirs à proximité</a></li>-->
						<!--<li><a href="#seminaires">5 - Offre Séminaire</a></li>-->
						<li><a href="#contact">Nous contacter</a></li>
					</ul>
				</div>
				
			</div>
		</div>
		
		<ul id="topics">
