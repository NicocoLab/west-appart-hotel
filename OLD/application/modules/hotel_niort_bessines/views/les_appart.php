<li id="apparts">
	<div class="scrollDetectMiddle"></div>
	
	<div class="carous">
		<div class="carouswrapper">
			
			<!-- LES STUDIOS -->
			<div id="three" class="slide">
				<h1 class="center">Les <span>Studios</span></h1>
				<div class="copy_wraper">
					<div class="image shadow">
						<a href="<?php echo base_url(); ?>design/wah/sudio-lit.jpg" class="fancybox" data-fancybox-group="apparts_sudios" title="Lit double">
							<img src="<?php echo base_url(); ?>design/wah/sudio-lit_thumb.jpg" alt="Lit double" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>					
						</a>
					</div>
					
					<div class="image shadow">
						<a href="<?php echo base_url(); ?>design/wah/sudio-cuisine.jpg" class="fancybox" data-fancybox-group="apparts_sudios" title="Cuisine équipée">
							<img src="<?php echo base_url(); ?>design/wah/sudio-cuisine_thumb.jpg" alt="Cuisine équipée" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
						</a>
					</div>
					
					<div class="image shadow">
						<a href="<?php echo base_url(); ?>design/wah/sudio-douche.jpg" class="fancybox" data-fancybox-group="apparts_sudios" title="Douche confort">
							<img src="<?php echo base_url(); ?>design/wah/sudio-douche_thumb.jpg" alt="Douche confort" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
						</a>
					</div>
					
					<div class="image shadow">
						<a href="<?php echo base_url(); ?>design/wah/sudio-tete-lit.jpg" class="fancybox" data-fancybox-group="apparts_sudios" title="Pour des nuits sereines">
							<img src="<?php echo base_url(); ?>design/wah/sudio-tete-lit_thumb.jpg" alt="Pour des nuits sereines" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
						</a>
					</div>
					
					<div class="image shadow">
						<a href="<?php echo base_url(); ?>design/wah/sudio-bureau.jpg" class="fancybox" data-fancybox-group="apparts_sudios" title="Bureau, wifi comprise">
							<img src="<?php echo base_url(); ?>design/wah/sudio-bureau_thumb.jpg" alt="Bureau, wifi comprise" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
						</a>
					</div>
					
					<div class="image shadow">
						<a href="<?php echo base_url(); ?>design/wah/sudio-mobilite-reduite.jpg" class="fancybox" data-fancybox-group="apparts_mobilite" title="Studio accessibilité pour les personnes à mobilité réduite">
							<img src="<?php echo base_url(); ?>design/wah/sudio-mobilite-reduite_thumb.jpg" alt="Studio accessibilité pour les personnes à mobilité réduite" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
						</a>
						<div style="display: none">
							<a href="<?php echo base_url(); ?>design/wah/sudio-mobilite-reduite-sejour.jpg" class="fancybox" data-fancybox-group="apparts_mobilite"></a>
							<a href="<?php echo base_url(); ?>design/wah/sudio-mobilite-reduite-douche.jpg" class="fancybox" data-fancybox-group="apparts_mobilite"></a>
						</div>
					</div>
				</div>
			</div>
			
			<!-- ACCUEIL LES APPARTS' -->
			<div id="one" class="slide active">
				<h1 class="center">Découvrez, <span>choisissez !</span></h1>
				<div class="copy_wraper">
					<div class="room">
						<h2>Les studios</h2>
						<a href="#three" class="slideto" title="Découvrez nos studios"><img src="<?php echo base_url()?>design/wah/appart-hotel-les-studios_thumb.jpg" alt="Découvrez nos studios" class="shadow" /></a>
					</div>
					<div class="room">
						<p class="shadow">
							Tous les appartements sont très bien isolés et insonorisés, entièrement équipés et prêts à vivre avec salle de bain spacieuse, WC séparés ; TV écran plat 82 cm avec USB vidéo, et connexion wifi gratuite.<br /><br />
							Ils disposent chacun d’une kitchenette aménagée (vaisselle complète, plats, casseroles, bouilloire, cafetière, micro-onde, réfrigérateur, plaque vitrocéramique), c'est-à-dire tout pour préparer de votre petit déjeuner jusqu’à votre dîner !<br /><br /> 
							Les appartements de 50m² disposent de deux chambres avec chacune un écran TV et un bureau. L’espace de vie comporte la kitchenette ainsi qu’un salon avec canapé et TV écran plat.
						</p>
					</div>
					<div class="room">
						<h2>Les appartements T3</h2>
						<a href="#two" class="slideto" title="Découvrez nos appartements T3"><img src="<?php echo base_url()?>design/wah/appart-hotel-les-appartements-t3_thumb.jpg" alt="Découvrez nos appartements T3" class="shadow" /></a>
					</div>
				</div>
			</div>
			
			<!-- LES APPARTEMENTS T3 -->
			<div id="two" class="slide">
				<h1 class="center">Les <span>Appartements T3</span></h1>
				<div class="copy_wraper">
					<div class="image">
						<a href="<?php echo base_url(); ?>design/wah/appartement_lit.jpg" class="fancybox" data-fancybox-group="apparts_t3" title="Lit double dans chaque chambre">
							<img src="<?php echo base_url(); ?>design/wah/appartement_lit_thumb.jpg" alt="Lit double dans chaque chambre" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
						</a>
					</div>
					
					<div class="image shadow">
						<a href="<?php echo base_url(); ?>design/wah/appartement_cuisine.jpg" class="fancybox" data-fancybox-group="apparts_t3" title="Cuicine équipée">
							<img src="<?php echo base_url(); ?>design/wah/appartement_cuisine_thumb.jpg" alt="Cuicine équipée" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
						</a>
					</div>
					
					<div class="image shadow">
						<a href="<?php echo base_url(); ?>design/wah/appartement_douche.jpg" class="fancybox" data-fancybox-group="apparts_t3" title="Douche confort">
							<img src="<?php echo base_url(); ?>design/wah/appartement_douche_thumb.jpg" alt="Douche confort" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
						</a>
					</div>
					
					<div class="image">
						<a href="<?php echo base_url(); ?>design/wah/appartement_bureau.jpg" class="fancybox" data-fancybox-group="apparts_t3" title="Bureau, wifi comprise">
							<img src="<?php echo base_url(); ?>design/wah/appartement_bureau_thumb.jpg" alt="Bureau, wifi comprise" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
						</a>
					</div>
					
					<div class="image">
						<a href="<?php echo base_url(); ?>design/wah/appartement_chambre.jpg" class="fancybox" data-fancybox-group="apparts_t3" title="Chambre tout confort">
							<img src="<?php echo base_url(); ?>design/wah/appartement_chambre_thumb.jpg" alt="Chambre tout confort" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
						</a>
					</div>
					
					<div class="image shadow">
						<a href="<?php echo base_url(); ?>design/wah/appartement_sejour.jpg" class="fancybox" data-fancybox-group="apparts_t3" title="Séjour agréable et confortable">
							<img src="<?php echo base_url(); ?>design/wah/appartement_sejour_thumb.jpg" alt="Séjour agréable et confortable" />
							<span class="zoom corner shadow"><img src="<?php echo base_url(); ?>design/icones/icoZoom.gif" alt="Agrandir l'image"/></span>
						</a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<a id="prev_appart" class="slideprev slideto" href="#three"></a>
	<a id="next_appart" class="slidenext slideto" href="#two"></a>
	
</li>