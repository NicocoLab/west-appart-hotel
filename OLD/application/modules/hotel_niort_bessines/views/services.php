<li id="services" rel="3">
	<div class="scrollDetectMiddle"></div>
		<h1 class="center"><span>Services et activités </span>proposés sur le site : </h1>
		 
		<div class="service_wraper">
			<div class="activite">
				<h2>Restaurant</h2>
				<h3>(pour groupe sur réservation)</h3>
				<img src="<?php echo base_url()?>design/wah/hotel-services-restaurant.jpg" alt="" class="shadow" />
			</div>
			<div class="activite">
				<h2>Découverte Kart</h2>
				<h3>(Session 10min.)</h3>
				<img src="<?php echo base_url()?>design/wah/hotel-services-session-kart.jpg" alt="" class="shadow" />
			</div>
			<div class="activite">
				<h2>Challenges kart</h2>
				<h3>(Entreprises / CE / particuliers)</h3>
				<img src="<?php echo base_url()?>design/wah/hotel-services-challenge.jpg" alt="" class="shadow" />
			</div>
		</div>
		
		<div class="infos">Karts toute dernière génération équipés de silencieux (ouverture : 9h30 à 12h30 et de 13h30 à 20h30)</infos>

</li>