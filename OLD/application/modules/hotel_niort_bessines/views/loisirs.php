<li id="loisirs">
	<div class="scrollDetectMiddle"></div>
	
	<h1 class="center">Loisirs à <span>proximité</span></h1>
	
	<div class="copy_wraper">
		
		<?php
			for ($i = 1; $i < 7; $i++)
			{
				echo '
						<div class="image">
							<a href="'.base_url().'design/wah/hotel_niort_karting_'.$i.'.jpg" class="fancybox" data-fancybox-group="kart" title="Hôtel niort Karting">
								<img src="'.base_url().'design/wah/hotel_niort_karting_'.$i.'_thumb.jpg" alt="Hôtel niort Karting"/>' .
									'<span class="zoom corner shadow"><img src="'.base_url().'design/icones/icoZoom.gif" alt="Agrandir l\'image"/></span>
							</a>
						</div>';
			}
		?>

	</div>
	
</li>