<?php
	echo form_open('');
		
		echo '<h2>Formulaire de contact</h2>';
		
		echo '<div class="field">'.form_label('Nom * :').form_input(array('name'=>'nom'), set_value('nom')).form_error('nom').'</div>';
		
		echo '<div class="field">'.form_label('Prénom :').form_input(array('name'=>'prenom'), set_value('prenom')).form_error('prenom').'</div>';
		
		echo '<div class="field">'.form_label('Société :').form_input(array('name'=>'societe'), set_value('societe')).form_error('societe').'</div>';
		
		echo '<div class="field">'.form_label('Adresse :').form_textarea(array('name'=>'adresse'), set_value('adresse')).form_error('adresse').'</div>';
		
		echo '<div class="field">'.form_label('Téléphone :').form_input(array('name'=>'telephone'), set_value('telephone')).form_error('telephone').'</div>';
		
		echo '<div class="field">'.form_label('E-mail * :').form_input(array('name'=>'email'), set_value('email')).form_error('email').'</div>';
		
		echo '<div class="field">'.form_label('Message * :').form_textarea(array('name'=>'message'), set_value('message')).form_error('message').'</div>';
		
		echo '<div class="submit">'.form_submit('valider', 'Valider').'</div>';
		
		echo '<p>* champs obligatoires</p>';
		
	echo form_close();
?>
