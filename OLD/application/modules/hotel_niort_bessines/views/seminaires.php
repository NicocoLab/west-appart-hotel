<li id="seminaires">
	<div class="scrollDetectMiddle"></div>
	
		<h1 class="center">Offre <span>séminaire</span></h1>
		<div class="service_wraper">
		
			<div class="activite shadow">
				<div class="switch">
					<div class="recto">
						<img src="<?php echo base_url()?>design/wah/hotel-seminaires.jpg" alt="Offre Séminaire" />
						<div class="legende">Salles de séminaire équipées et modulables</div>
						<span class="ico_next"></span>
					</div>
					<div class="verso">
						<h2>Offre Séminaire</h2>
						<p>
							3 salles modulables de 10 à 70 places<br /><br />
							<u>Equipements :</u>
						</p>
							<ul>
								<li>Connexion wifi</li>
								<li>Vidéo projecteur</li>
								<li>Ecran led de 120cm</li>
								<li>Paper-board</li>
								<li>Sono, micro</li>
							</ul>
						<p>
							<br />(Tarifs sur demande)
						</p>
					</div>
				</div>
			</div>
			
			<div class="sep"></div>
			
			<div class="activite shadow">
				<div class="switch">
					<div class="recto">
						<img src="<?php echo base_url()?>design/wah/hotel-services-restaurant.jpg" alt="Restaurant La carambole" />
						<div class="legende">Salle de restauration avec terrasse ombragée</div>
						<span class="ico_next"></span>
					</div>
					<div class="verso">
						<h2>Offre restauration</h2>
						<p>
							Salle de réception 80 places<br />
							(pour groupe sur réservation)
							<br /><br />
							Buffet traiteur / cocktail dinatoire<br />
							Petit déjeuner / Open bar
							<br /><br />
							Possibilité de location de la salle (sous condition)<br />
							(Tarifs sur demande)
						</p>
					</div>
				</div>
			</div>
			
			<div class="sep"></div>
			
			<div class="activite shadow">
				<div class="switch">
					<div class="recto">
						<img src="<?php echo base_url()?>design/wah/hotel-seminaires-challenge.jpg" alt="Speed Fun Karting" />
						<div class="legende">Organisation de challenge entreprise</div>
						<span class="ico_next"></span>
					</div>
					<div class="verso">
						<h2>Offre Speed Fun Karting</h2>
						<p>
							Formule découverte ou formule challenge<br /><br />
							Accessible aux débutants comme aux confirmés, le défoulement et les sensations sont garanties sur cette piste rapide et technique et toujours avec des karts dernière génération<br /><br />
							(Tarifs sur demande)
						</p>
					</div>	
				</div>
			</div>
		</div>
</li>