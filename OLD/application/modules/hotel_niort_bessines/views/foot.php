		
		</ul>
	
		<div id="footer">
			<div class="innerWrapper">
				
				<div id="text-next">
					<div id="text" class="shadow">
						<!--
						<div class="foot_accueil">
							<p><span>West Appart'Hotel</span> a été conçu pour vous accueillir et vous faciliter la vie lors de vos séjours, qu’ils soient professionnels ou de loisirs.</p>
							<p><span>West Appart'Hotel</span> apporte une réponse parfaitement adaptée aux besoins croissants des entreprises à héberger leurs collaborateurs en déplacement, mais également aux particuliers à forte mobilité et aux touristes en famille ou entre amis...</p>
							<p>Ce nouveau concept d'hébergement en court ou long séjour est classé en Gamme Très Bon Confort par CléVacances.</p>
							<p>L'idée : Vous sentir comme <span>chez vous</span> et en toute autonomie !</p>
						</div>
						-->
						
						<div class="foot_apparts" style="display: none">
							<p><span>West Appart'Hotel</span> présente à la location des appartements joliment décorés, équipés et prêts à vivre avec tous les services hôteliers classiques, mais également des prestations et des services à la carte et adaptés à vos besoins.</p>
							<p><span>West Appart'Hotel</span> fait l’objet d’une attention particulière afin de vous proposer un espace convivial, fonctionnel et chaleureux où la liberté et le confort sont les fondements de notre engagement.</p>
						</div>
						
						<div class="foot_loisirs" style="display: none">
							<p>Une piste de <span>karting loisirs</span> est à proximité de l'hôtel pour les avides de sensations fortes.</p>
							<p>Celle-ci n’engendre <span>aucune nuisance sonore</span> pour les apparts (tarifs préférentiels pour les intéressés !). </p>
						</div>
						
						<div class="foot_seminaires" style="display: none">
							<p>Grand complexe de 1800m<sup>2</sup> unique dans la région juste à la sortie de Niort sur l'axe de La Rochelle, <span>West Appart'Hotel</span> vous propose 4 prestations sur le même site : Salle de séminaire / Restauration / Karting / Hébergement.</p>
							<p>Facilitez vos organisations : <span>1 seule adresse, 1 seul interlocuteur.</span></p>
							<p>Package affaire à partir de 60 € HT (Salle de réunion, déjeuner, open bar, 30mn de karting)</p>
						</div>
					</div>
					
					<div id="next"><a href="#apparts">Continuer</a></div>
				</div>
				
				<div class="foot-infos">
					<a href="#" id="cle_vacances"></a>
					<a href="#" class="picto"><img src="<?php echo base_url(); ?>design/wah/picto/wifi.png" alt="Wifi gratuit et illimité dans tout l'hôtel"></a>
					<a href="#" class="picto"><img src="<?php echo base_url(); ?>design/wah/picto/video.png" alt="Etablissement sous vidéo-surveillance"></a>
					<a href="#" class="picto"><img src="<?php echo base_url(); ?>design/wah/picto/parking.png" alt="Parking gratuit, sécurisé et ombragé"></a>
					<a href="#" class="picto"><img src="<?php echo base_url(); ?>design/wah/picto/handi.png" alt="Accessible aux personnes à mobilité réduite"></a>
					<a href="#" class="picto"><img src="<?php echo base_url(); ?>design/wah/picto/asce.png" alt="Accès aux étages possible par ascenseur"></a>
				</div>
			</div>
		</div>
		
		<script src="<?php echo base_url(); ?>js/libs.js"></script>
		<script src="<?php echo base_url(); ?>js/swipe.js"></script>
		<script src="<?php echo base_url(); ?>js/wah.js?v120905-3"></script>
		
		<script type="text/javascript">
			  var _gaq = _gaq || [];
			  _gaq.push(['_setAccount', 'UA-7357947-2']);
			  _gaq.push(['_trackPageview']);
			
			  (function() {
			    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();
		</script>
		
		<!--  -->
		
	</body>
</html>
