<li id="reservations">
	<div class="scrollDetectMiddle"></div>
	
	<div class="carous">
		<div class="carouswrapper">
			
			<!-- TARIFS -->
			<div id="five" class="slide">
				<h1 class="center">Nos <span>Tarifs</span></h1>
				<div class="copy_wraper">
					<div class="tarifs corner shadow">
			
						<h2>Tarifs hébergements TTC</h2>
						
						<table>
							<tr class="first">
								<td></td>
								<td>A partir de 2 nuits</td>
								<td>A partir de 3 nuits</td>
								<td>A partir de 4 nuits</td>
								<td>Forfait Week-end<br />A partir de 2 nuits</td>
							</tr>
							<tr>
								<td class="lab">Studio 25m<sup>2</sup></td>
								<td class="prix">70,00 € /nuit</td>
								<td class="prix">65,00 € /nuit</td>
								<td class="prix">60,00 € /nuit</td>
								<td class="prix">60,00 € /nuit</td>
							</tr>
							<tr>
								<td class="lab">Studio 35m<sup>2</sup></td>
								<td class="prix">90,00 € /nuit</td>
								<td class="prix">85,00 € /nuit</td>
								<td class="prix">75,00 € /nuit</td>
								<td class="prix">75,00 € /nuit</td>
							</tr>
							<tr>
								<td class="lab">Appart. 50m<sup>2</sup></td>
								<td class="prix">120,00 € /nuit</td>
								<td class="prix">115,00 € /nuit</td>
								<td class="prix">110,00 € /nuit</td>
								<td class="prix">110,00 € /nuit</td>
							</tr>
						</table>
						
						<p>
							<b>Inclus dans la location  :</b>
							Draps, petite serviette, drap de bain, tapis de douche, essuie mains, essuie verre, 
							Distributeurs de savon, gel douche et liquide vaisselle
						</p>	
						<p class="etoile">
							
							* Option ménage : 50 €
						</p>	
						<p>
							<b>Prestations gratuites :</b> Accès internet, Parking ombragé.
							<span class="pets">Les animaux ne sont pas acceptés.</span>
						</p>	
					</div>
				</div>
			</div>
			
			<!-- ACCUEIL RESERVATIONS -->
			<div id="four" class="slide active">
				<h1 class="center">Réservez <span>votre appart'</span></h1>
				<div class="copy_wraper">
					<div class="resa_accueil corner">
						<a href="#five" class="slideto" title="Nos tarifs"><span>Nos tarifs</span></a>
					</div>
					<div class="resa_accueil resa_num corner shadow">
						<p>
							Pour une réservation simple et rapide, contactez-nous au<br /><br /><span>07 86 67 02 50</span>
							<br />
							ou
							<br />
							<a href="http://poitou-charentes-meuble.for-system.com/f34317_fr-.aspx" target="_blank" title="Réservez votre chambre en ligne maintenant !">Réservez en ligne</a>
						</p>
					</div>
					<div class="resa_accueil corner">
						<a href="#six" class="slideto" title="Informations pratiques"><span>Informations pratiques</span></a>
					</div>
				</div>
			</div>
			
			<!-- INFOS -->
			<div id="six" class="slide">
				<h1 class="center">Informations <span>pratiques</span></h1>
				<div class="copy_wraper">
					
					<div class="resainfos corner shadow">
						<h2>Services</h2>
						<p>
							Les petits déjeuners, comme les repas sont à prendre dans les chambres, nous ne disposons pas de service de restauration vu qu’il s’agit de logements équipés à cet effet.
						</p>
						<div class="sep-h"></div>
						<p>
							Dans chaque location, des dosettes de café, infusion et de thé vous seront offertes pour agrémenter votre installation.
						</p>
						<!--
						<div class="sep-h"></div>
						<p>
							Des distributeurs de plats cuisinés, sandwichs, viennoiseries et de boissons sont à votre disposition dans le hall d’accueil.
						</p>
						-->
					</div>
					
					<div class="resainfos small corner shadow">
						<h2>Horaires d’ouverture de l’accueil :</h2>
						<p>
							Du lundi au vendredi de 17h00 à 19h00<br />
							Accueil téléphonique au 07 86 67 02 50 tous les jours de 9h00 à 20h00
						</p>
						<div class="sep-h"></div>
						<p>
							 L'accès au site et aux appartements se fait par digicode 24h/24h, celui-ci vous sera transmis par SMS le jour de votre arrivée et vous permettra de prendre possession de votre location à l'heure de votre choix
						</p>
						<div class="sep-h"></div>
						<p>
							L’établissement est sous Vidéo-surveillance
						</p>
					</div>
					
				</div>
			</div>
	
		</div>
	</div>
	
	<a id="prev_resa" class="slideprev slideto" href="#five"></a>
	<a id="next_resa" class="slidenext slideto" href="#six"></a>
</li>
