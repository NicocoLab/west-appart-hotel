--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: a_resa_chambre; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE a_resa_chambre (
    ref_reservation integer NOT NULL,
    ref_chambre integer NOT NULL,
    prix_par_nuits numeric(15,6),
    taux_tva numeric(6,2) DEFAULT 0,
    digicode numeric(6,0)
);


ALTER TABLE public.a_resa_chambre OWNER TO nicolas;

--
-- Name: a_resa_options; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE a_resa_options (
    ref_reservation integer NOT NULL,
    ref_option integer NOT NULL,
    date timestamp without time zone,
    taux_tva numeric(10,6) DEFAULT 0
);


ALTER TABLE public.a_resa_options OWNER TO nicolas;

--
-- Name: adresses; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE adresses (
    ref_client integer NOT NULL,
    ref_type integer NOT NULL,
    "desc" character varying,
    cp character varying,
    ville character varying,
    pays character varying
);


ALTER TABLE public.adresses OWNER TO nicolas;

--
-- Name: chambres; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE chambres (
    id integer NOT NULL,
    lib character varying NOT NULL,
    nb_places integer DEFAULT 0 NOT NULL,
    prix_ht numeric(20,6) DEFAULT 0 NOT NULL,
    enabled boolean DEFAULT true,
    ref_tva integer,
    categorie integer,
    surface numeric(5,2) DEFAULT 0
);


ALTER TABLE public.chambres OWNER TO nicolas;

--
-- Name: COLUMN chambres.categorie; Type: COMMENT; Schema: public; Owner: nicolas
--

COMMENT ON COLUMN chambres.categorie IS '1 = studio | 2 = Appart | 3 => Salle de séminaire | 4 = Salle de réception';


--
-- Name: chambres_id_seq; Type: SEQUENCE; Schema: public; Owner: nicolas
--

CREATE SEQUENCE chambres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chambres_id_seq OWNER TO nicolas;

--
-- Name: chambres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nicolas
--

ALTER SEQUENCE chambres_id_seq OWNED BY chambres.id;


--
-- Name: chambres_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nicolas
--

SELECT pg_catalog.setval('chambres_id_seq', 18, true);


--
-- Name: civilites; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE civilites (
    id integer NOT NULL,
    lib character varying,
    ordre integer DEFAULT 999
);


ALTER TABLE public.civilites OWNER TO nicolas;

--
-- Name: civilites_id_seq; Type: SEQUENCE; Schema: public; Owner: nicolas
--

CREATE SEQUENCE civilites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.civilites_id_seq OWNER TO nicolas;

--
-- Name: civilites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nicolas
--

ALTER SEQUENCE civilites_id_seq OWNED BY civilites.id;


--
-- Name: civilites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nicolas
--

SELECT pg_catalog.setval('civilites_id_seq', 5, true);


--
-- Name: clients; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE clients (
    id integer NOT NULL,
    ref_civilite integer,
    nom character varying,
    prenom character varying,
    email character varying NOT NULL,
    password character varying NOT NULL,
    espace_client_enabled boolean DEFAULT true
);


ALTER TABLE public.clients OWNER TO nicolas;

--
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: nicolas
--

CREATE SEQUENCE clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clients_id_seq OWNER TO nicolas;

--
-- Name: clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nicolas
--

ALTER SEQUENCE clients_id_seq OWNED BY clients.id;


--
-- Name: clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nicolas
--

SELECT pg_catalog.setval('clients_id_seq', 12, true);


--
-- Name: options; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE options (
    id integer NOT NULL,
    lib character varying NOT NULL,
    "desc" character varying,
    prix_ht numeric(20,6),
    ref_tva integer,
    enabled boolean DEFAULT true
);


ALTER TABLE public.options OWNER TO nicolas;

--
-- Name: options_id_seq; Type: SEQUENCE; Schema: public; Owner: nicolas
--

CREATE SEQUENCE options_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.options_id_seq OWNER TO nicolas;

--
-- Name: options_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nicolas
--

ALTER SEQUENCE options_id_seq OWNED BY options.id;


--
-- Name: options_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nicolas
--

SELECT pg_catalog.setval('options_id_seq', 2, true);


--
-- Name: reservations; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE reservations (
    id integer NOT NULL,
    debut timestamp without time zone NOT NULL,
    fin timestamp without time zone NOT NULL,
    ref_client integer NOT NULL,
    paiement_recu boolean DEFAULT false,
    paiement_card_number character varying,
    paiement_trans_id character varying,
    paiement_effective_amount integer,
    paiement_warranty_result boolean,
    date_creation timestamp without time zone,
    heure_arrive integer DEFAULT 1
);


ALTER TABLE public.reservations OWNER TO nicolas;

--
-- Name: reservations_id_seq; Type: SEQUENCE; Schema: public; Owner: nicolas
--

CREATE SEQUENCE reservations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reservations_id_seq OWNER TO nicolas;

--
-- Name: reservations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nicolas
--

ALTER SEQUENCE reservations_id_seq OWNED BY reservations.id;


--
-- Name: reservations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nicolas
--

SELECT pg_catalog.setval('reservations_id_seq', 43, true);


--
-- Name: telephones; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE telephones (
    ref_client integer NOT NULL,
    number character varying,
    ref_type integer NOT NULL
);


ALTER TABLE public.telephones OWNER TO nicolas;

--
-- Name: test; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE test (
    id integer NOT NULL,
    date timestamp without time zone
);


ALTER TABLE public.test OWNER TO nicolas;

--
-- Name: test_reseption_seq; Type: SEQUENCE; Schema: public; Owner: nicolas
--

CREATE SEQUENCE test_reseption_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_reseption_seq OWNER TO nicolas;

--
-- Name: test_reseption_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nicolas
--

ALTER SEQUENCE test_reseption_seq OWNED BY test.id;


--
-- Name: test_reseption_seq; Type: SEQUENCE SET; Schema: public; Owner: nicolas
--

SELECT pg_catalog.setval('test_reseption_seq', 14, true);


--
-- Name: tva; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE tva (
    id integer NOT NULL,
    taux numeric(5,2) NOT NULL,
    "order" integer DEFAULT 999
);


ALTER TABLE public.tva OWNER TO nicolas;

--
-- Name: tva_id_seq; Type: SEQUENCE; Schema: public; Owner: nicolas
--

CREATE SEQUENCE tva_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tva_id_seq OWNER TO nicolas;

--
-- Name: tva_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nicolas
--

ALTER SEQUENCE tva_id_seq OWNED BY tva.id;


--
-- Name: tva_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nicolas
--

SELECT pg_catalog.setval('tva_id_seq', 2, true);


--
-- Name: types_tel; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE types_tel (
    id integer NOT NULL,
    lib character varying,
    "order" integer DEFAULT 999
);


ALTER TABLE public.types_tel OWNER TO nicolas;

--
-- Name: type_tel_id_seq; Type: SEQUENCE; Schema: public; Owner: nicolas
--

CREATE SEQUENCE type_tel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_tel_id_seq OWNER TO nicolas;

--
-- Name: type_tel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nicolas
--

ALTER SEQUENCE type_tel_id_seq OWNED BY types_tel.id;


--
-- Name: type_tel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nicolas
--

SELECT pg_catalog.setval('type_tel_id_seq', 4, true);


--
-- Name: types_adresses; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE types_adresses (
    id integer NOT NULL,
    lib character varying,
    "order" integer DEFAULT 999
);


ALTER TABLE public.types_adresses OWNER TO nicolas;

--
-- Name: types_adresses_id_seq; Type: SEQUENCE; Schema: public; Owner: nicolas
--

CREATE SEQUENCE types_adresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.types_adresses_id_seq OWNER TO nicolas;

--
-- Name: types_adresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nicolas
--

ALTER SEQUENCE types_adresses_id_seq OWNED BY types_adresses.id;


--
-- Name: types_adresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nicolas
--

SELECT pg_catalog.setval('types_adresses_id_seq', 2, true);


--
-- Name: users; Type: TABLE; Schema: public; Owner: nicolas; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    nom character varying,
    login character varying NOT NULL,
    pwd character varying NOT NULL,
    email character varying,
    enabled boolean DEFAULT true
);


ALTER TABLE public.users OWNER TO nicolas;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: nicolas
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO nicolas;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nicolas
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nicolas
--

SELECT pg_catalog.setval('users_id_seq', 3, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY chambres ALTER COLUMN id SET DEFAULT nextval('chambres_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY civilites ALTER COLUMN id SET DEFAULT nextval('civilites_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY clients ALTER COLUMN id SET DEFAULT nextval('clients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY options ALTER COLUMN id SET DEFAULT nextval('options_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY reservations ALTER COLUMN id SET DEFAULT nextval('reservations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY test ALTER COLUMN id SET DEFAULT nextval('test_reseption_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY tva ALTER COLUMN id SET DEFAULT nextval('tva_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY types_adresses ALTER COLUMN id SET DEFAULT nextval('types_adresses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY types_tel ALTER COLUMN id SET DEFAULT nextval('type_tel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: a_resa_chambre; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY a_resa_chambre (ref_reservation, ref_chambre, prix_par_nuits, taux_tva, digicode) FROM stdin;
1	1	100.000000	19.60	\N
9	1	10.330000	5.50	\N
10	2	57.692307	19.60	\N
11	2	57.692307	19.60	\N
12	1	10.330000	5.50	\N
12	2	57.692307	19.60	\N
13	1	10.330000	5.50	\N
13	2	57.692307	19.60	\N
14	2	57.692307	19.60	\N
15	2	57.692307	19.60	\N
16	2	57.692307	19.60	\N
17	2	57.692307	19.60	\N
18	2	57.692307	19.60	\N
19	2	57.692307	19.60	\N
20	1	10.330000	5.50	\N
21	1	10.330000	5.50	\N
22	1	10.330000	5.50	\N
24	2	57.692307	19.60	\N
23	1	10.330000	5.50	897808
25	1	10.330000	5.50	\N
26	1	57.692707	19.60	\N
26	6	91.137123	19.60	\N
27	2	57.692307	19.60	\N
27	4	57.692707	19.60	\N
28	1	57.692707	19.60	\N
28	2	57.692307	19.60	\N
29	6	91.137123	19.60	\N
30	5	57.692707	19.60	\N
31	3	57.690000	19.60	\N
32	10	57.692707	19.60	\N
33	7	91.137123	19.60	\N
34	4	57.692707	19.60	\N
35	8	57.692707	19.60	\N
35	15	57.692707	19.60	\N
35	17	91.137123	19.60	\N
36	9	57.692307	19.60	\N
37	18	91.137123	19.60	\N
38	16	57.692707	19.60	\N
41	14	57.692707	19.60	\N
43	13	57.692707	19.60	\N
\.


--
-- Data for Name: a_resa_options; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY a_resa_options (ref_reservation, ref_option, date, taux_tva) FROM stdin;
\.


--
-- Data for Name: adresses; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY adresses (ref_client, ref_type, "desc", cp, ville, pays) FROM stdin;
1	1	10 Ter Rue des Cosses\n\nChardonchamps	86000	POITIERS	France
1	2	ZI République 2\n\n12 Rue Eugène Chevreul	86000	POITIERS	
2	1	10 Ter rue des cosses	86000	POITIERS	FRANCE
2	2				
3	1				
3	2				
4	1				
4	2	gfhghghgfh			
5	1				
5	2				
6	1				
6	2				
7	1				
7	2				
8	1	rue de la chance	86000	POITEIRS	FRANCE
9	1	Rue de la pas de chance	86000	POITEIRS	FRANCE
10	1	sdfsdfsdfsdf	86000	POITEIRS	FRANCE
11	1	18 Rue des rue	86000	POITEIRS	
12	1	dsfdsfdf	86000	POITEIRS	
\.


--
-- Data for Name: chambres; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY chambres (id, lib, nb_places, prix_ht, enabled, ref_tva, categorie, surface) FROM stdin;
6	Chambre 6	6	91.137123	t	1	2	50.00
2	Chambre 2	2	57.692307	t	1	1	25.00
3	Chambre 3	2	57.690000	t	1	1	25.00
4	Chambre 4	2	57.692707	t	1	1	25.00
5	Chambre 5	2	57.692707	t	1	1	25.00
7	Chambre 7	6	91.137123	t	1	2	50.00
8	Chambre 8	2	57.692707	t	1	1	25.00
1	Chambre 1	2	57.692707	t	1	1	25.00
10	Chambre 10	2	57.692707	t	1	5	35.00
9	Chambre 9	2	57.692307	t	1	1	25.00
11	Chambre 11	2	57.692707	t	1	1	25.00
12	Chambre 12	2	57.692707	t	1	1	25.00
13	Chambre 13	2	57.692707	t	1	1	25.00
14	Chambre 14	2	57.692707	t	1	1	25.00
15	Chambre 15	2	57.692707	t	1	1	25.00
16	Chambre 16	2	57.692707	t	1	1	25.00
18	Chambre 18	6	91.137123	t	1	2	50.00
17	Chambre 17	6	91.137123	t	1	2	50.00
\.


--
-- Data for Name: civilites; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY civilites (id, lib, ordre) FROM stdin;
2	Mr	0
1	Mr et Mme	1
4	Mme	2
\.


--
-- Data for Name: clients; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY clients (id, ref_civilite, nom, prenom, email, password, espace_client_enabled) FROM stdin;
1	1	TEST	test	test@test.com	d32955eaf478539fc61f4aacdf3cdd14b51431e4	t
2	2	Rivière	Nicolas	nriviere@extrabat.org	c32ebc046d51c0999460d895b11a7bb8fd8a2fd3	t
3	2	Rivière 2	Nicolas	nriviere@extrabat.org	c32ebc046d51c0999460d895b11a7bb8fd8a2fd3	t
5	2	Rivière 3	Essai	nico.yanis@gmail.com	c32ebc046d51c0999460d895b11a7bb8fd8a2fd3	t
4	2	Rivière 2	test	nico.riv@free.fr	0m337YOhWycAfNCkcpC1u0LeDKDhVSOmsF+JLYJizny4QXjM78/na1UE6Pn2a4MRHKvBIyrem1JtkYuvFJGBLw==	t
6	2	toto	toto	toto@toto.com	A3EhlRvyMd1zeyVp8+O6EBk0E4+aPvgiJboKJPbeYwVpLUNf+8kubZX1N+TA8uOfr/JvFx9mXLuzeIp6SXNTlQ==	t
7	2	VAUZELLE	Francis	tot@roro.com	DVVDo+Wq4d0wbbxzlcmixP2hTmzrMlVY9OdVMHZ+eQE7TXUfFgf2Q6JVnWYeHVG2/QVl905N7thKamG6MfvBFQ==	t
8	2	PASQUET	Karen	pasquet@pasquet.com	JFj4QopQYgNNRBDn4XDQAhsiQdvVdjZit0CFmskVaHneknOZVTYqlxmFFguhRlKAHiMutf7LrXgVgCovQdO/dg==	t
9	2	PASQUET	Karen	pasquet@pasquet.fr	KNBnC5cl0Hfu+BWoLuI2DcBMoEW2UYG6a+gnAROEHrTF78w5Wiwv48BjPaXqrTgFkF1CautWFPLGGckWTkpJFg==	t
10	2	PASQUET 3	Karen	pasquet@pasquet.org	v7eDfdjP+7nSccW+8eTAwguABmkWszp3f38YcIYpedl5r9g6dVjfOU+PqhY3t+9Cvp0q4YNwCsaeHdkqLNJv/Q==	t
11	2	Test 3	Nicolas	tata@tata.com	wO8CN7zzOuUZmCPPU0JvEnMnrnF2+igW72W2NodZHB/rOXWXVJp3kjjENf9K7sjKz/orTD9Xcq5gp3DkDmkTsA==	t
12	2	titi	Karen	titi@titi.com	CFgBxzj7lJURn8A6cRIqXCcF+enmdoBrt7xdzgt88djOOmVnbMlVEvTQi/jOzIkzPHeW7GUbEjbiniFk0NzSxQ==	t
\.


--
-- Data for Name: options; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY options (id, lib, "desc", prix_ht, ref_tva, enabled) FROM stdin;
1	Petit déj	Avec la compote, la confiote, et la trippote !\n\nah ah !	100.000000	1	t
2	Ménage	On nettoie même les capotes usées !	10.000000	1	t
\.


--
-- Data for Name: reservations; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY reservations (id, debut, fin, ref_client, paiement_recu, paiement_card_number, paiement_trans_id, paiement_effective_amount, paiement_warranty_result, date_creation, heure_arrive) FROM stdin;
1	2012-03-12 00:00:00	2012-03-15 00:00:00	1	f	\N	\N	\N	\N	\N	1
9	2012-05-03 00:00:00	2012-05-04 00:00:00	6	f	\N	\N	\N	\N	\N	1
10	2012-04-01 00:00:00	2012-04-01 00:00:00	6	f	\N	\N	\N	\N	\N	1
11	2012-05-04 00:00:00	2012-05-04 00:00:00	6	f	\N	\N	\N	\N	\N	1
12	2012-02-01 00:00:00	2012-02-01 00:00:00	6	f	\N	\N	\N	\N	\N	1
13	2012-01-02 00:00:00	2012-01-02 00:00:00	6	f	\N	\N	\N	\N	\N	1
14	2012-02-29 00:00:00	2012-02-29 00:00:00	6	f	\N	\N	\N	\N	\N	1
15	2012-02-02 00:00:00	2012-02-02 00:00:00	6	f	\N	\N	\N	\N	\N	1
16	2012-02-03 00:00:00	2012-02-03 00:00:00	6	f	\N	\N	\N	\N	\N	1
17	2012-02-04 00:00:00	2012-02-04 00:00:00	6	f	\N	\N	\N	\N	\N	1
18	2012-02-05 00:00:00	2012-02-05 00:00:00	6	f	\N	\N	\N	\N	\N	1
19	2012-02-06 00:00:00	2012-02-06 00:00:00	6	f	\N	\N	\N	\N	\N	1
20	2012-05-14 00:00:00	2012-05-14 00:00:00	6	f	\N	\N	\N	\N	\N	1
21	2012-02-13 00:00:00	2012-02-13 00:00:00	6	f	\N	\N	\N	\N	\N	1
22	2012-08-02 00:00:00	2012-08-04 00:00:00	6	f	\N	\N	\N	\N	\N	1
23	2012-05-17 00:00:00	2012-05-17 00:00:00	6	f	\N	\N	\N	\N	\N	1
24	2012-05-17 00:00:00	2012-05-17 00:00:00	6	f	\N	\N	\N	\N	\N	1
25	2012-05-01 00:00:00	2012-05-01 00:00:00	6	f	\N	\N	\N	\N	2012-05-18 00:09:08	1
26	2012-06-08 00:00:00	2012-06-08 00:00:00	10	f	\N	\N	\N	\N	2012-06-08 00:10:39	1
27	2012-06-08 00:00:00	2012-06-08 00:00:00	6	t	497010XXXXXX0000	000058	13800	t	2012-06-08 14:23:45	1
28	2012-06-10 00:00:00	2012-06-10 00:00:00	6	f	\N	\N	\N	\N	2012-06-10 13:01:38	1
29	2012-06-10 00:00:00	2012-06-10 00:00:00	6	f	\N	\N	\N	\N	2012-06-10 13:12:27	1
30	2012-06-10 00:00:00	2012-06-10 00:00:00	1	t	497010XXXXXX0003	000112	6900	t	2012-06-10 13:16:55	1
31	2012-06-10 00:00:00	2012-06-10 00:00:00	1	f	\N	\N	\N	\N	2012-06-10 16:01:33	1
32	2012-06-10 00:00:00	2012-06-10 00:00:00	11	f	\N	\N	\N	\N	2012-06-10 21:11:25	1
33	2012-06-10 00:00:00	2012-06-10 00:00:00	12	t	497010XXXXXX0001	000114	10900	t	2012-06-10 21:14:46	1
34	2012-06-10 00:00:00	2012-06-10 00:00:00	12	t	497010XXXXXX0007	000115	6900	f	2012-06-10 21:55:03	3
35	2012-06-10 00:00:00	2012-06-10 00:00:00	12	t	497010XXXXXX0001	000116	24700	t	2012-06-10 21:57:04	1
36	2012-06-10 00:00:00	2012-06-10 00:00:00	12	t	497010XXXXXX0009	000117	6900	t	2012-06-10 21:59:07	1
37	2012-06-10 00:00:00	2012-06-10 00:00:00	12	f	\N	\N	\N	\N	2012-06-10 22:26:38	1
38	2012-06-10 00:00:00	2012-06-10 00:00:00	12	f	\N	\N	\N	\N	2012-06-10 22:40:27	1
41	2012-06-10 00:00:00	2012-06-10 00:00:00	12	t	497010XXXXXX0000	000122	6900	t	2012-06-10 22:46:45	1
43	2012-06-10 00:00:00	2012-06-10 00:00:00	12	f	\N	\N	\N	\N	2012-06-10 22:58:47	1
\.


--
-- Data for Name: telephones; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY telephones (ref_client, number, ref_type) FROM stdin;
1	06 50 71 11 86	1
1	05 49 88 95 50	3
2	05 49 88 95 50	3
2		1
3		3
3		1
4		3
4		1
5		3
5		1
6		3
6		1
7		3
7		1
8	06 50 71 11 86	1
8	05 49 88 95 50	3
9	06 50 71 11 86	1
9	05 49 88 95 50	3
10	06 50 71 11 86	1
10		3
11	06 50 71 11 86	1
11	05 49 88 95 50	3
12	06 50 71 11 86	1
12		3
\.


--
-- Data for Name: test; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY test (id, date) FROM stdin;
1	2012-05-04 13:48:39
2	2012-05-04 13:55:28
3	2012-05-04 14:19:02
4	2012-05-04 14:19:12
5	2012-05-04 14:19:19
6	2012-05-04 14:23:57
7	2012-05-04 14:32:15
8	2012-05-04 14:55:17
9	2012-05-04 14:56:27
10	2012-05-04 15:13:23
11	2012-05-04 15:16:30
12	2012-05-04 18:04:30
13	2012-05-09 16:30:49
14	2012-05-17 21:35:41
\.


--
-- Data for Name: tva; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY tva (id, taux, "order") FROM stdin;
1	19.60	0
2	5.50	1
\.


--
-- Data for Name: types_adresses; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY types_adresses (id, lib, "order") FROM stdin;
1	Domicile	0
2	Professionel	1
\.


--
-- Data for Name: types_tel; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY types_tel (id, lib, "order") FROM stdin;
1	Téléphone Mobile	0
3	Téléphone fixe	1
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: nicolas
--

COPY users (id, nom, login, pwd, email, enabled) FROM stdin;
1	Nicolas	nicolas	222008c599cefe6b1888f68b262915a291cfd83c	nico.riv@free.rf	t
2	testr	logloglog	b8627be8fb4590537cad578c168a0ac9f38a463b	nico.riv@free.fr	t
3	Francis	francis	2d681a3441dc03c735b1311636a060b871e445c7		t
\.


--
-- Name: chambres_pkey; Type: CONSTRAINT; Schema: public; Owner: nicolas; Tablespace: 
--

ALTER TABLE ONLY chambres
    ADD CONSTRAINT chambres_pkey PRIMARY KEY (id);


--
-- Name: civilites_pkey; Type: CONSTRAINT; Schema: public; Owner: nicolas; Tablespace: 
--

ALTER TABLE ONLY civilites
    ADD CONSTRAINT civilites_pkey PRIMARY KEY (id);


--
-- Name: clients_pkey; Type: CONSTRAINT; Schema: public; Owner: nicolas; Tablespace: 
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);


--
-- Name: options_pkey; Type: CONSTRAINT; Schema: public; Owner: nicolas; Tablespace: 
--

ALTER TABLE ONLY options
    ADD CONSTRAINT options_pkey PRIMARY KEY (id);


--
-- Name: reservations_pkey; Type: CONSTRAINT; Schema: public; Owner: nicolas; Tablespace: 
--

ALTER TABLE ONLY reservations
    ADD CONSTRAINT reservations_pkey PRIMARY KEY (id);


--
-- Name: tva_pkey; Type: CONSTRAINT; Schema: public; Owner: nicolas; Tablespace: 
--

ALTER TABLE ONLY tva
    ADD CONSTRAINT tva_pkey PRIMARY KEY (id);


--
-- Name: type_tel_pkey; Type: CONSTRAINT; Schema: public; Owner: nicolas; Tablespace: 
--

ALTER TABLE ONLY types_tel
    ADD CONSTRAINT type_tel_pkey PRIMARY KEY (id);


--
-- Name: types_adresses_pkey; Type: CONSTRAINT; Schema: public; Owner: nicolas; Tablespace: 
--

ALTER TABLE ONLY types_adresses
    ADD CONSTRAINT types_adresses_pkey PRIMARY KEY (id);


--
-- Name: users_login_key; Type: CONSTRAINT; Schema: public; Owner: nicolas; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_login_key UNIQUE (login);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: nicolas; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: a_resa_chambre_ref_chambre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY a_resa_chambre
    ADD CONSTRAINT a_resa_chambre_ref_chambre_fkey FOREIGN KEY (ref_chambre) REFERENCES chambres(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: a_resa_chambre_ref_reservation_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY a_resa_chambre
    ADD CONSTRAINT a_resa_chambre_ref_reservation_fkey FOREIGN KEY (ref_reservation) REFERENCES reservations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: a_resa_options_ref_option_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY a_resa_options
    ADD CONSTRAINT a_resa_options_ref_option_fkey FOREIGN KEY (ref_option) REFERENCES options(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: a_resa_options_ref_reservation_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY a_resa_options
    ADD CONSTRAINT a_resa_options_ref_reservation_fkey FOREIGN KEY (ref_reservation) REFERENCES reservations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: adresses_ref_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY adresses
    ADD CONSTRAINT adresses_ref_client_fkey FOREIGN KEY (ref_client) REFERENCES clients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: adresses_ref_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY adresses
    ADD CONSTRAINT adresses_ref_type_fkey FOREIGN KEY (ref_type) REFERENCES types_adresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: chambres_ref_tva_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY chambres
    ADD CONSTRAINT chambres_ref_tva_fkey FOREIGN KEY (ref_tva) REFERENCES tva(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: clients_ref_civilite_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_ref_civilite_fkey FOREIGN KEY (ref_civilite) REFERENCES civilites(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: options_ref_tva_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY options
    ADD CONSTRAINT options_ref_tva_fkey FOREIGN KEY (ref_tva) REFERENCES tva(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: reservations_ref_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY reservations
    ADD CONSTRAINT reservations_ref_client_fkey FOREIGN KEY (ref_client) REFERENCES clients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: telephones_ref_client_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY telephones
    ADD CONSTRAINT telephones_ref_client_fkey FOREIGN KEY (ref_client) REFERENCES clients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: telephones_ref_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: nicolas
--

ALTER TABLE ONLY telephones
    ADD CONSTRAINT telephones_ref_type_fkey FOREIGN KEY (ref_type) REFERENCES types_tel(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

