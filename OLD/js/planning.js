(function($) {
	
	var params, $me;
	
	// ============================================================================================================== Le Plugin
	$.fn.planning = function( method ) {
	
		if ( typeof method === 'object' || ! method )
		{
			return planning.init.apply( this, arguments );
		}
		else
		{
			$.error( 'Method ' +  method + ' does not exist or is not accessible on jQuery.diapo' );
		}
	}
	
	// ============================================================================================================== Les méthodes
	planning =
	{
		init: function(options)
		{
			$me = $(this);
			params = $.extend({}, $.fn.planning.defaults, options);
			
			// Les filtres
			planning.make_filtre();
			
			// On lance le plugin pour chaque mois a afficher
			for (var i = 0, debut; i < params.nb_mois; i++)
			{
				debut = new Date(params.debut.getFullYear(), (params.debut.getMonth() + parseInt(i)), 1);
				planning.create(debut);
			}
		},
		
		// ---------------------
		
		make_filtre: function()
		{
			var options;
			for (var i in params.month)
			{
				var sel = i == params.debut.getMonth() + 1 ? 'selected="selected"' : '';
				options += '<option value="'+i+'" '+sel+'>'+params.month[i]+'</option>';
			}
			
			var years;
			for (var i = params.debut.getFullYear() - 3; i < params.debut.getFullYear() + 4; i++)
			{
				var sel = i == params.debut.getFullYear() ? 'selected="selected"' : '';
				years += '<option value="'+i+'" '+sel+'>'+i+'</option>';
			}
			
			var $filtre = $('<form action="http://wah.com/planning_reservations/index" method="post" accept-charset="utf-8" class="filtre">' +
								 		'<label>Mois :</label>' +
								 		'<select name="mois">'+options+'</select>' +
								 		'<label>Année :</label>' +
								 		'<select name="annee">'+years+'</select>' +
								 '</form>'
								);
			
			$filtre.appendTo($me);
			
			$filtre.find('select[name=mois]').on('change', function() {
				var mois = $(this).find('option:selected').val();
				params.debut.setMonth(mois - 1);
				$me.find('.wpl').remove()
				for (var i = 0, debut; i < params.nb_mois; i++)
				{
					debut = new Date(params.debut.getFullYear(), (params.debut.getMonth() + parseInt(i)), 1);
					planning.create(debut);
				}
			})
			
			$filtre.find('select[name=annee]').on('change', function() {
				var annee = $(this).find('option:selected').val();
				params.debut.setFullYear(annee);
				$me.find('.wpl').remove()
				for (var i = 0, debut; i < params.nb_mois; i++)
				{
					debut = new Date(params.debut.getFullYear(), (params.debut.getMonth() + parseInt(i)), 1);
					planning.create(debut);
				}
			})
		},
		
		// ---------------------
		
		create: function (debut)
		{
			var $plan = $('<div class="wpl"></div>');
			
			$('<h2>'+params.month[debut.getMonth()+1]+' '+debut.getFullYear()+'</h2>').appendTo($plan);
			
			planning.create_ligne($plan, debut, '', 1)
			
			$plan.appendTo($me)
			
			params.chambre_width	= $plan.find('.wpl_chambre:first').width() + parseInt($plan.find('.wpl_chambre:first').css('padding-left').replace('px', ''));
			params.day_width		= $plan.find('.wpl_day:first').width() + 1;
			
			planning.get_reservations($plan, debut);
		},
		
		// ---------------------
		
		create_ligne: function($plan, date, chambre_lib, show_jours)
		{
			var nb_jours = new Date(date.getFullYear(), date.getMonth()+1, -1).getDate()+1;
			
			var $ligne = $('<div class="wpl_ligne"/>').css({position: 'relative'}).appendTo($plan);
			
			$('<div class="wpl_chambre'+(show_jours == 1 ? '' : ' b_top')+'">'+chambre_lib+'</div>').appendTo($ligne);
			
			for (var i = 1, j; i <= 31; i++)
			{
				j = i <= nb_jours ? i : '';
				
				$('<div class="wpl_day b_top b_left">'+(show_jours == 1 ? j : '')+'</div>').appendTo($ligne);
			}
						
			return $ligne;
		},
		
		// ---------------------
		
		get_reservations: function($plan, debut)
		{
			
				$.ajax({
					url: params.get_reservations_url+(debut.getMonth()+1)+'/'+debut.getFullYear(),
					success: function(data)
					{
						var resa = $.parseJSON(data);
						
						$.each(params.chambres, function(i){
							planning.display($plan, debut, params.chambres[i], resa);
						})
					}
				})
			
		},
		
		// ---------------------
		
		display: function($plan, debut, ch, resa)
		{
			var $ligne = planning.create_ligne($plan, debut, ch.lib), cl;
			
			$.each(resa, function(i)
			{
				if (ch.id == resa[i].chambre_id)
				{
					var date = resa[i].debut.split(' ');
					date = date[0].split('-');
					var left = ((date[2] - 1) * params.day_width) + params.chambre_width + 2;
					
					var fin = resa[i].fin.split(' ');
					fin = fin[0].split('-');
					var width = ((fin[2] - date[2] + 1) * params.day_width) - 3;
					
					var height = $ligne.height() - 3;
					
					cl = resa[i].paiement == 't' ? 'green' : 'red';
					
					var $resa = $('<div class="wpl_resa '+cl+'"></div>');
					
					$resa.css({position: 'absolute', width: width, left: left, top: '2px', height: height, cursor: 'pointer'}).appendTo($ligne);
					
					$resa.on('click', function()
					{
						planning.show_resa($resa, resa[i]);
						return false;
					});
				}
			})
		},
		
		// ---------------------
		
		show_resa: function($resa, resa)
		{
			var $layer = $('<div class="layer"></div>').appendTo('html');
			
			var $popin, offset = $resa.offset();
			
			var debut = resa.debut.split(' ');
			debut = debut[0].split('-');
			debut = debut[2] +'/'+ debut[1] +'/'+ debut[0];
			
			var fin = resa.fin.split(' ');
			fin = fin[0].split('-');
			fin = fin[2] +'/'+ fin[1] +'/'+ fin[0];
			
			$popin = $('<div style="width: 400px; position: absolute; display: none; z-index: 9999" class="popinfo">' +
								'<div class="popinfoContenu corner shadow" style="width: 380px; padding: 10px">' +
									'<a href="#" class="close" title="Fermer"></a>' +
									'<span class="objet">'+resa.civ+' '+resa.nom+' '+resa.prenom+'</span>' +
									'<span class="popindate">Du '+debut+' au '+fin+'</span>' +
									'<span class="popindate">Prix : '+resa.ca+' € TTC</span>' +
									'<span class="boutons"><a href="'+params.base_url+'reservations_list/detail/'+resa.id+'" class="modif">Voir le detail</a></span>' +
								'</div>' +
								'<div class="popinfoArrow"></div>' +
						  '</div>');
							  
			$popin.appendTo('body');
			$popin.css({top: offset.top - $popin.height(), left: offset.left - 200 + ($resa.width()/2)});
			$popin.fadeIn('fast');
			
			$popin.find('.close').on('click', function(){planning.popin_remove($popin, $layer); return false;})
			$layer.on('click', function(){planning.popin_remove($popin, $layer); return false;})
			
			$(document).bind('keydown', function(e) {
				if (e.keyCode == 27) {
					e.preventDefault();
					planning.popin_remove($popin, $layer);
				}
			});
		},
		
		// ---------------------
		
		popin_remove: function($popin, $layer)
		{
			$popin.fadeOut('fast', function(){
				$popin.remove();
				$layer.remove();
				$(document).unbind('keydown');
			});
		}
	}
	
	// ============================================================================================================== Options
	$.fn.planning.defaults = {
		chambres: {},
		debut: new Date(),
		nb_mois : 3,
		
		get_reservations_url: '',
		
		month: {1 : "Janvier", 2 : "Février", 3 : "Mars", 4 : "Avril", 5 : "Mai", 6 : "Juin", 7 : "Juillet", 8 : "Août", 9 : "Septembre", 10 : "Octobre", 11 : "Novembre", 12 : "Décembre"}
	};
	
})(jQuery)