(function($) {
	
	var params, $me, $control, play;
	
	// ============================================================================================================== Le Plugin
	$.fn.diapo = function( method ) {
	
		if ( typeof method === 'object' || ! method )
		{
			return $.diapo.init.apply( this, arguments );
		}
		else
		{
			$.error( 'Method ' +  method + ' does not exist or is not accessible on jQuery.diapo' );
		}
	}
	
	// ============================================================================================================== Les méthodes
	$.diapo =
	{
		init: function( options )
		{
			$me = $(this);
			params = $.extend({}, $.fn.diapo.defaults, options);
			
			// Parse les images et les cache
			$me.find('a').each(function(){
				params.images.push({
					src: $(this).find('img').attr('src'),
					bt: $(this).attr('href'),
					txt: $(this).find('img').attr('alt'),
				})
			}).hide()
			
			// Il faut que la largeur soit divisible par le nombre de slot horizontaux
			var width = parseInt($(this).width());
			while ((width % params.nb_slots_h) != 0)
			{
				width--;
			}
			
			// Les paramètres du plugin -- A améliorer : la hauteur doit être la moitié de la largeur, donc les images aussi...
			params.elwidth = width;
			params.elheight = width / 2;
			params.slots_per_image = params.nb_slots_h * params.nb_slots_v;
			params.slots_width = params.elwidth / params.nb_slots_h;
			params.slots_height = params.elheight / params.nb_slots_v;
			params.current_img = 0;
			params.last_image = 0;
			params.animation_on = false;
			
			// Ajuste la div
			var total_width = (params.elwidth * 100) / 75;
			var booking_width =  total_width * 25 / 100;
			var control_height = params.elheight * 10 / 100;
			
			$('#slide_wrapper').css({width : total_width+'px', height: params.elheight+'px'})
			$me.css({height: params.elheight+'px', width: params.elwidth+'px'})
			$('#booking').css({width : booking_width+'px', height: params.elheight+'px'})
			
			// Prépare le loader et les control
			var $loader = $('<div id="diapo_loader">Chargement</div>');
			var $layer_control = $('<div id="'+params.control_layer_id+'"></div>').css({height: control_height+'px'});
			$control = $('<div id="'+params.control_div_id+'"></div>').css({display:'none'});
			
			$loader.appendTo($me);
			
			// Lance la "fabrication" des slots
			for (var i in params.images)
			{
				$.diapo.make_slots(params.images[i], i);
			}
			
			// On met la première image comme image de fond
			$('<img src="'+params.images[0].src+'" />').css({height: params.elheight+'px', width: params.elwidth+'px'}).appendTo($me);
			
			// On affiche
			$loader.fadeOut().remove();
			$layer_control.appendTo($me).fadeIn();
			$control.appendTo($me).css({height: control_height+'px'}).fadeIn();
			
			// Pour l'autoplay, l'interval mini doit être du temps total de l'animatiion d'une image
			var interval_min = params.nb_slots_v * (params.next_slots_time + params.animate_time);
			var interval = params.interval < interval_min ? interval_min : params.interval;
			
			play = params.autoplay === true ? setInterval('$.diapo.show_image()', interval) : '';
		},
		
		// ==============================================================================================================
		/*
		 *  Alors là, c'est pas facile de commenter !
		 * 
		 *  L'idée, c'est qu'on fait une div par image
		 *  Dans cette div, on créer une div par slot
		 *  Dans le slot, on va inclure une div qui contiens l'image
		 *  et on va positioner l'image en fonction de la largeur des slots qui la précède, et de la "rangé" dans laquelle le slot se trouve
		 * 
		 */
		
		make_slots: function(image, index)
		{
			var $div, $maindiv, $slot, i = 1, x = 0, y = 0;
			
			$div = $('<div id="diapo_image_'+index+'"></div>').css({width: params.elwidth+'px', height: params.elheight+'px', position: 'absolute', top: 0, left: 0, overflow: 'hidden'});
			
			while (i <= params.slots_per_image)
			{
				$maindiv = $('<div></div>').css({position: 'relative', width: params.slots_width+'px', height: params.slots_height+'px', 'float': 'left', overflow: 'hidden'})
				
				$slot = $('<div class="slot slot_'+i+'"><img src="'+image.src+'" /></div>');
				$slot.css({
					width: params.slots_width+'px',
					height: params.slots_height+'px',
					position: 'absolute',
					top: -params.slots_width,
					left: -params.slots_height,
					overflow: 'hidden'
				});
				
				$slot.find('img').css({
					position: 'absolute',
					left : '-'+x+'px',
					top: '-'+y+'px',
					width: params.elwidth+'px',
					height: params.elheight+'px'
				})
				
				$slot.appendTo($maindiv);
				$maindiv.appendTo($div);
				
				if (x == (params.elwidth - params.slots_width))
				{
					x = 0;
					y = y + params.slots_height;
				}
				else
				{
					x = x + params.slots_width;
				}

				i++;
			}
			
			$div.appendTo($me);
			$.diapo.make_bt(image.bt, index);
			
		},
		
		// ==============================================================================================================
		
		make_bt: function(bt, index)
		{
			var $bt = $('<a href="#"><img src="'+bt+'"></a>').css({'z-index': 999, height: '90%'}).appendTo($control);
			
			$bt.find('img').css({height: '100%', 'margin-top': '5%'})
			
			$bt.on('click', function(){
				clearInterval(play);
				if (params.animation_on === false && params.current_img != index)
				{
					$.diapo.show_image(index);
				}
				return false;
			});
		},
		
		// ==============================================================================================================
		/*
		 * 
		 * Autant c'était dur de commenter avant, autant là c'est impossible !
		 * 
		 * Le mieux pour comprendre, c'est de suivre le déplacement de la variable e
		 * 
		 * Si on a 10x5 slot :
		 * 
		 *  1  2  3  4  5  6  7  8  9 10
		 * 11 12 13 14 15 16 17 18 19 20
		 * 21 22 23 24 25 26 27 28 29 30
		 * 31 32 34 34 35 36 37 38 39 40
		 * 41 42 43 44 45 46 47 48 49 50
		 * 
		 * Au premier tour, on anime que le slot_1
		 * Au deuxième, les slots 2 (slot_1++) et 11 (2 + (10-1)) -- 10 étant le nombre de slot , et le temps d'animation sera x 2
		 * 
		 * Ainsi de suite jusqu'à 50 
		 * 
		 */
		
		
		show_image: function(image)
		{
			var index, col = 1, z, c, e, cont = true;
			
			params.animation_on = true;
			params.last_image = params.current_img;
			
			$('#diapo_image_'+params.last_image).css({'z-index' : 5})
			params.current_img  = parseInt(params.current_img) + parseInt(1)
			
			params.current_img = params.current_img == (params.images.length) ? 0 : params.current_img;
			
			if (image !== undefined)
			{
				index = image;
				params.current_img = image;
			}
			else
			{
				index = params.current_img;
			}
			
			$('#diapo_image_'+index).css({'z-index' : 9})
			
			$('#debug').append('<br />image'+index)

			c = params.sens == 'right' ? 1 : params.slots_per_image;
			
			while (cont === true)
			{
				z = 0;
				e = c;
				
				while (z < col)
				{
					if (params.sens == 'right')
					{
						setTimeout("$.diapo.show_slot("+index+","+e+", "+params.last_image+", '"+params.sens+"')", c*params.next_slots_time)
						cont = e == params.slots_per_image ? false : true;
						e = e + (params.nb_slots_h - 1);
					}
					else
					{
						setTimeout("$.diapo.show_slot("+index+","+e+", "+params.last_image+", '"+params.sens+"')", (params.slots_per_image - (c-1))*params.next_slots_time)
						cont = e == 0 ? false : true;
						e = e - (params.nb_slots_h - 1);
					}
					
					z++;
				}
				
				if (col < params.nb_slots_v)
				{
					col++;
				}
				
				c = params.sens == 'right' ? c+1 : c-1; 
			}
			
			params.sens = params.sens == 'right' ? 'left' : 'right';
			
			
		},
		
		// ==============================================================================================================
		/*
		 * RAS
		 * 
		 * On repositionne l'image précédente à la fin de l'animation
		 * 
		 */
		
		show_slot: function(index, z, toclear, sens)
		{
			$('#diapo_image_'+index+' .slot_'+z).animate({top: '0', left: '0'}, params.animate_time, function(){
				
				if (z == params.slots_per_image && sens == 'right')
				{
					params.animation_on = false;
					$.diapo.reset_image(toclear);
				}
				else if (z == 1 && sens == 'left')
				{
					params.animation_on = false;
					$.diapo.reset_image(toclear);
				}
				
			});
				
		},
		
		// ==============================================================================================================
		
		reset_image: function(index)
		{
			$('#diapo_image_'+index).css({'z-index' : 1})
			$('#diapo_image_'+index+' .slot').each(function(){
				$(this).css({top: -params.slots_width, left: -params.slots_height})
			});
			$('#debug').append('<br />reset '+index)
		}
	}
	
	
	// ============================================================================================================== Options
	$.fn.diapo.defaults = {
		images: [],
		nb_slots_h: 10,
		nb_slots_v: 5,
		sens: 'right',
		autoplay: true,
		animate_time: 800,
		next_slots_time: 200,
		interval: 5000,
		control_layer_id: 'diapo_control_layer',
		control_div_id: 'diapo_control',
	};
	
})(jQuery)