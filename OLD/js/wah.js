
($.fn.scrollTo = function(){
	if (this.length > 0)
	{
		$("#topics:not(:animated)").stop().animate({ top: $(this).position().top }, 1500);
	}
})(jQuery);



$(document).ready(function(){

	// Les text shadow
	$('h1.center, .copy_wraper .room h2').textShadow();
	
	// La navigation
	$('#nav a').on('click', function(){
		nav_to($(this).attr('href').replace('#', ''))
		$('#nav .actif').parent().next('li').html() === null ? $('#next a').addClass('retour').text('Retour') : $('#next a').removeClass('retour').text('Continuer')
		return false;
	})
	
	$('#next a').on('click', function(){
		
		var next = $('#nav .actif').parent().next('li').html() === null ? 'accueil' : $('#nav .actif').parent().next('li').find('a').attr('href').replace('#', '')
		nav_to(next)
		
		$('#nav .actif').parent().next('li').html() === null ? $('#next a').addClass('retour').text('Retour') : $('#next a').removeClass('retour').text('Continuer')
		
		return false
	})
	
	$('#logo').on('click', function(){
		nav_to(1);
		return false
	})
	
	function nav_to(to)
	{
		$('#nav a').removeClass('actif')
		$('#nav a[href=#'+to+']').addClass('actif')
		
		var topTarget  = $('#'+to).position().top * -1;
		$("#topics").animate({ top: topTarget }, 1000);
	}
	
	
	$('.scrollDetectMiddle').bind('inview', function (event, visible) {
		if (visible) {
			var rel = $(this).parent().attr('id');
			
			$('#text').find('div:visible').hide();
			if($('#text').find('.foot_'+rel).html() === null)
			{
				$('#text').fadeOut();
			}
			else
			{
				$('#text').show().find('.foot_'+rel).show();
			}
		}
	});
	
	/* ===== Popin footer ===== */
	
	$('.picto').on('click', function() {open_popin($(this), true); return false;})
	
	function open_popin($a, show_image)
	{
		$me = $('body');
		var offset = $a.offset();
		
		var text = $a.find('img').attr('alt');
		
		var $layer = $('<div class="layer" style="position: absolute; top: 0; left:0; width: 100%; height: '+$('body').height()+'px; z-index: 9998"></div>').appendTo($me);
		$layer.on('click', function(){close_popin($popin, $layer);})
		
		var $popin = $('<div style="width: 200px; position: absolute; display: none; z-index: 9999" class="popinfo">' +
								'<a href="#" class="close" title="Fermer"></a>' +
								
								'<div class="popinfoContenu corner shadow">' +
									(show_image === true ? '<img src="'+($a.find('img').attr('src').replace('.png', '_black.png'))+'" style="float: left; margin-right: 5px"/>' : '') +
									'<p style=" vertical-align: middle;">' + text + '</p>' +
								'</div>' +
								
								'<div class="popinfoArrow"></div>' +
						  '</div>');
						  
		$popin.appendTo($me);
		
		$popin.css({top: offset.top - $popin.height() - 5, left: offset.left - 100 + ($a.width()/2)});
		$popin.fadeIn('fast');
		
		$popin.find('.close').on('click', function(){close_popin($popin, $layer); return false;})
		
		$(document).bind('keydown', function(e) {
			if (e.keyCode == 27) {
				e.preventDefault();
				close_popin($popin, $layer);
			}
		});
		
		$popin.find('.boutons a').on('click', function(){
			close_popin($popin, $layer)
			if ($(this).hasClass('noinstall'))
			{
				return false;
			}
		})
	}
	
	// --------------------------------
	
	function close_popin($popin, $layer)
	{
		$popin.remove();
		$layer.remove();
		$(document).unbind('keydown');
	}
	
	
	/* ===== Navigation page apparts ===== */
	$('.carous .slide').css({width: $(window).width(), left: $(window).width() * -1});
	$(window).resize(function() {$('.carous .slide').css({width: $(window).width()})});
	
	var topappart = $('#apparts').offset().top
	$('.slideto').on('click', function(){
		
		var to = $(this).attr('href');
		
		var $goto = $(to);
		
		var tmpleft = $goto.position().left * -1
		
		$('.carouswrapper').animate({left: tmpleft}, 1000);
		
		
		if (to == '#one')
		{
			$('#prev_appart').fadeIn().attr('href', '#three');
			$('#next_appart').fadeIn().attr('href', '#two')
		}
		
		if (to == '#two')
		{
			$('#next_appart').fadeOut();
			$('#prev_appart').attr('href', '#one')
		}
		
		if (to == '#three')
		{
			$('#prev_appart').fadeOut();
			$('#next_appart').attr('href', '#one')
		}
		
		
		if (to == '#four')
		{
			$('#prev_resa').fadeIn().attr('href', '#five');
			$('#next_resa').fadeIn().attr('href', '#six')
		}
		
		if (to == '#five')
		{
			$('#prev_resa').fadeOut();
			$('#next_resa').attr('href', '#four')
		}
		
		if (to == '#six')
		{
			$('#next_resa').fadeOut();
			$('#prev_resa').attr('href', '#four')
		}
		
		return false
	})
	
	$(".fancybox").fancybox({helpers: {title: null}});
	
	$('#topics').wipetouch({
			allowDiagonal: false,
			wipeDown: function(result) {$('#next a').trigger('click')},
			wipeUp: function(result) {$('#next a').trigger('click')},
			wipeLeft: function() {alert('left')},
			preventDefault: true
		});
		
		
	// flips
	$('.switch').on('click', function(){
		
		var elem = $(this);
		
		if(elem.data('switch'))
		{
			elem.animate({left : 0}).data('switch',false)
		}
		else
		{
			elem.animate({left : -(elem.width() / 2)+'px'}).data('switch',true);
		}
	})
	
	
	var form, $form;
	
	// first load
	
	form = $.ajax({url: 'http://west-appart-hotel.com/hotel_niort_bessines/contact', async: false}).responseText;
	
	$form = $(form);
	
	$form.appendTo('#contact_form')
	
	attach_submit($form)
	
	function attach_submit($f)
	{
		$f.on('submit', function(){
		
			$('#contact_form').empty()
			
			form = $.ajax({
						type: 'post',
						url: 'http://west-appart-hotel.com/hotel_niort_bessines/contact',
						data: $(this).serialize(),
						async: false
					}).responseText;
			
			$form = $(form);
			
			$form.appendTo('#contact_form')
			
			attach_submit($form)
			
			return false;
		})
	}
	
});
