(function($){
	
	var $that, start, img_width, params;
	
	$.fn.roll = function(options){
	
		$that = $(this);
		var div_width = $that.width();
		
		/* Paramètres -------------*/
		params = $.extend({
			delay : 5,
			duration : 1000,
		},options);
		
		
		/* Cacher les images -------------*/
		$.each($that.find('.roll'), function() {
			$(this).hide();
		});

		/* Positionne l'image 1 -------------*/
		var $img = $that.find('img:first');
		img_width = $img.width();
		
		$img.css({
			'position': 'absolute',
			'left': ((div_width - $img.width()) / 2),
		}).show().addClass('active');
		
		/* Valeur de départ pour l'animate -------------*/
		start = div_width + (($img.width() - div_width) / 2);
		end = (div_width - (($img.width() - div_width) / 2)) * -1;
		
		/* Positionne les autres images -------------*/
		$.each($that.find('.roll').not(':first'), function() {
			$(this).css({'position':'absolute', 'left':start});
			$(this).show();
		});
		
		/* Lance l'animation -------------*/
		setInterval(
			function(){
				go_next()
			},
			(params.delay*1000));
	}
	
	
	
	go_next = function() {
		
		var $to_roll;
		
		//alert( $that.children.length )
		$last = $that.find("img:last");
		
		
		if($that.find('img.active').is(":last-child"))
		{
			$to_roll = $that.find('img:first');
			$('#debug').append('1<br>')
		}
		else
		{
			$to_roll = $that.find('img.active').next();
			$('#debug').append('2<br>')
		}
		
		
		$that.find('img').each(function(){
			$(this).removeClass("active").css({'z-index':'0'});
		})
		
		$to_roll.addClass("active").css({'z-index':'99'});
		
		
		$to_roll.animate({'left': '-='+img_width}, params.duration, function(){
			
			$.each($that.find('img').not('.active'), function() {
				$(this).css({'position':'absolute', 'left':start});
			});
			
		});
		
		
		
	}
	
	
	
})(jQuery)