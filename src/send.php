<?php

error_reporting(E_ALL); // todo : changer en prod
ini_set('display_errors','1'); // todo : changer en prod

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
	exit(json_encode(['success' => false, 'code' => 1]));
}

$fields = ['nom', 'prenom', 'societe', 'adresse', 'telephone', 'email', 'message'];

$from = null;
$message = '';

foreach ($fields as $field) {
	$message .= $field.' : '.(isset($_POST[$field]) ? trim(strip_tags($_POST[$field])) : '').'<br>';

	if ($field == 'email') {
		$from = trim(strip_tags($_POST[$field]));
	}
}

if (is_null($from)) {
	exit(json_encode(['success' => false, 'code' => 2]));
}

// -------------------------------------

require 'phpmailer/class.phpmailer.php';
require 'phpmailer/class.smtp.php';

// -------------------------------------

$mailer = new PHPMailer(true);

$mailer->isSMTP();                                      // Active l'envoi via SMTP
$mailer->Host = 'ssl://srv37.haisoft.net';                                      // Specifie le serveur SMTP à utiliser.
$mailer->SMTPAuth = true;                               // Active l'authentification par SMTP
$mailer->Username = 'sender@west-appart-hotel.com';        // Nom d'utilisateur SMTP. // todo : changer en prod
$mailer->Password = 'Jzrg82_0';         // Mot de passe SMTP // todo : changer en prod
$mailer->Port = 465;                                     // Port
$mailer->isHTML(true);                                  // Set email format to HTML

$mailer->From = $from;                            // L'adresse mail de l'emetteur du mail.
$mailer->addAddress('contact@west-appart-hotel.com');
$mailer->Subject = 'Nouveau message depuis site west-appart-hotel.com';
$mailer->Body = $message;

$success = $mailer->send();

echo json_encode(['success' => $success, 'code' => 3]);
